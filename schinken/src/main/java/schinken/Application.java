package schinken;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.util.Iterator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;



@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Application {

    public static void main(String[] args) throws org.json.simple.parser.ParseException {
        SpringApplication.run(Application.class, args);
        JSONschreiben();
        JSONlesen();
       
    }
    
    
    
    @SuppressWarnings("unchecked")
	public static void JSONschreiben()
    {
    	JSONObject obj = new JSONObject();
    	obj.put("name", "max");
    	obj.put("age", new Integer(18));
     
    	JSONArray numbers = new JSONArray();
    	numbers.add(new Integer(564));
    	numbers.add(new Integer(456));
    	numbers.add(new Integer(745));
     
    	obj.put("numbers", numbers);
     
    	try {
     
    		FileWriter file = new FileWriter("D:\\test.json");
    		file.write(obj.toJSONString());
    		file.flush();
    		file.close();
     
    	} catch (IOException e) {
    		e.printStackTrace();
    	}
     
    	System.out.print("it works!!! oder so \n");
     
    }
    
    public static void JSONlesen()
    {
    	JSONParser parser = new JSONParser();
    	 
    	try {
     
    		Person x = new Person();
    		Object obj = parser.parse(new FileReader("D:\\test.json"));
     
    		JSONObject jsonObject = (JSONObject) obj;
     
    		String name = (String) jsonObject.get("name");
    		System.out.println(name);
    		x.setName(name);
     
    		long age = (Long) jsonObject.get("age");
    		System.out.println(age);
    		x.setAge(age);
     
    		// loop array
    		JSONArray l = (JSONArray) jsonObject.get("numbers");
    		@SuppressWarnings("unchecked")
			Iterator<Long> iterator = l.iterator();
    		while (iterator.hasNext()) {
    			x.addNumbers(iterator.next());
    		}
    		System.out.println(x.toString());
    		
     
    	} catch (FileNotFoundException e) {
    		e.printStackTrace();
    	} catch (IOException e) {
    		e.printStackTrace();
    	} catch (org.json.simple.parser.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
}
