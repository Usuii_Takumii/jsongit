package schinken;

import java.util.ArrayList;

public class Person {

	
	public String name;
	public long age;
	public ArrayList<Long> numbers;
	
	public Person(String nnaem, int aeg)
	{
		this.name = nnaem;
		this.age = aeg;
		numbers = new ArrayList<Long>();
	}

	public Person() {
		numbers = new ArrayList<Long>();
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getAge() {
		return age;
	}
	
	public void setAge(long age)
	{
		this.age = age;
	}
	
	public void addNumbers(long number)
	{
		numbers.add(number);
		System.out.println(number);
	}
	
	public String toString()
	{
		String x = ""+name+" "+age+" ";
		for (int i = 0; i<numbers.size();i++)
		{
			x = x+" "+numbers.get(i);
		}
		
		return x;
		
	
	}


}